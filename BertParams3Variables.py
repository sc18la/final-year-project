from tabulate import tabulate
import math
from math import exp
from scipy.optimize import fsolve
import statistics

#Data used to determine parameters
timeData = [0, 0.4630, 0.9260, 1.389, 1.852, 2.315, 2.736, 3.7287, 4.3424,  6.4602, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]

#BERTALANFFY PARAMS - 3 Simultaneous Equations
def getBertParams(data1, data2, data3): #Calculates parameters g, h and i when given three data points
    def equations(vars):
        g, h, i = vars
        eq1 = (g/h -(g/h -i)*exp(-h*data1[0]/3))**3 - data1[1]
        eq2 = (g/h -(g/h -i)*exp(-h*data2[0]/3))**3 - data2[1]
        eq3 = (g/h -(g/h -i)*exp(-h*data3[0]/3))**3 - data3[1]
        return [eq1, eq2, eq3]
    g, h, i = fsolve(equations, (0.1, 0.05, 0.1))
    return g, h, i

def neighDataPoints(): #Chooses the neighbouring data points
    params = []
    for x in range(0, len(timeData)-2):
        data1 = [timeData[x], volumeData[x]]
        data2 = [timeData[x+1], volumeData[x+1]]
        data3 = [timeData[x+2], volumeData[x+2]]
        params.append(getBertParams(data1, data2, data3))
    return params

def averages():  
    paramG = []
    paramH = []
    paramI = []
    for x in neighDataPoints():
        paramG.append(x[0])
        paramH.append(x[1])
        paramI.append(x[2])
    meanG = sum(paramG)/len(paramG)
    meanH = sum(paramH)/len(paramH)
    meanI = sum(paramI)/len(paramI)
    medianG = statistics.median(paramG)
    medianH = statistics.median(paramH)
    medianI = statistics.median(paramI)
    return meanG, meanH, meanI, medianG, medianH, medianI



def bertVolEstimates(paramG, paramH, paramI): #Works out the estimated volumes using the given parameter
    vols = []
    for x in timeData:
        vols.append((paramG/paramH-(paramG/paramH - paramI)*exp(-paramH*x/3))**3)
    return(vols)
    
def residualsSquared(estiVols): #Calculates the squared difference between the estimated vols and measured vols
    resiArray = []
    for x in range(0,len(volumeData)):
        resiArray.append((estiVols[x]-volumeData[x])**2)
    return resiArray      


gParams = []
hParams = []
iParams = []
for i in range(0, len(neighDataPoints())):
    gParams.append(neighDataPoints()[i][0])
    hParams.append(neighDataPoints()[i][1])
    iParams.append(neighDataPoints()[i][2])
timesUsed = ["t0t1t2", "t1t2t3", "t2t3t4", "t3t4t5", "t4t5t6", "t5t6t7", "t6t7t8", "t7t8t9", "t8t9t10"]
print(tabulate({"Times Used": timesUsed, "g": gParams, "h": hParams, "i": iParams}, headers="keys"),"\n")

print("Using neighbouring data points to determine the parameters g, h and i")
for i in range(0,8):
    paramG = gParams[i]
    paramH = hParams[i]
    paramI = iParams[i]
    print("   g = ", paramG,"\n   h = ", paramH, "\n   i = ", paramI)
    print(tabulate({"Time": timeData,"Estimated Vol.": bertVolEstimates(paramG,paramH,paramI), "Residual Sqaured": residualsSquared(bertVolEstimates(paramG,paramH,paramI))}, headers="keys"),"\n")
    print("Residual Sum of Squares = ", sum( residualsSquared(bertVolEstimates(paramG,paramH,paramI))))
    print("\n---------------------------------------------------")


