import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import math

#DATA
timeData = [0, 0.463014, 0.926027, 1.38904, 1.85205, 2.31507, 2.73699, 3.728767, 4.342466,  6.460274, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
timeRange = 7.5
t = np.linspace(0,timeRange)
#plt.figure(figsize=(20, 12))
plt.xlabel('Time (years)', fontsize=15)
plt.ylabel(r'$Volume (x10^{-4} mm^3)$', fontsize=15)
plt.plot(timeData, volumeData, 'ro', linestyle='None')


#Chemotherapy Revised - Time Intervals
def chemoNoC(V, t): #Exponential model for no chemo drugs in the body
    expoParam = 1.5162879854524727
    dVdt = expoParam*V
    return dVdt
def chemoRevised(V, t): #Exponential model with a logarithmic decay rate for chemo drugs in the body
    expoParam = 1.5162879854524727
    doseParam = 16
    dVdt = expoParam*V - doseParam*V*math.log(t)
    return dVdt

V0 = 0.0001 #Initial tumour volume
t0 = 0
tMax = timeRange
chemoStartTimes = [6.0, 6.1, 6.2, 6.3, 6.4] #Array of times when chemotherapy is administered
chemoTimeLength = 0.038 #Duration of chemotherapy in the body

#Calculated volume and time arrays are joined together and plotted
chemoR = []
chemoRt = []

timeC = np.linspace(t0, chemoStartTimes[0])
chemoRt.append(timeC)
chemoR = odeint(chemoNoC, V0, timeC)

for i in range (0, len(chemoStartTimes)-1):
    timeC = np.linspace(chemoStartTimes[i], chemoStartTimes[i] + chemoTimeLength)
    chemoRt.append(timeC)
    chemoR = np.append(chemoR, odeint(chemoRevised, chemoR[-1], timeC))
    
    timeNoC = np.linspace(chemoStartTimes[i] + chemoTimeLength, chemoStartTimes[i+1])
    chemoRt.append(timeNoC)
    chemoR = np.append(chemoR, odeint(chemoNoC, chemoR[-1], timeNoC))

timeNoC = np.linspace(np.concatenate(chemoRt)[-1], timeRange)
chemoRt.append(timeNoC)
chemoR = np.append(chemoR, odeint(chemoNoC, chemoR[-1], timeNoC))

plt.plot(np.concatenate(chemoRt), chemoR, label="New Model - Chemotherapy - Revised", color='magenta')

plt.legend()
plt.show()
