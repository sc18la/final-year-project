import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import math

#DATA
timeData = [0, 0.463014, 0.926027, 1.38904, 1.85205, 2.31507, 2.73699, 3.728767, 4.342466,  6.460274, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
timeRange = 7
t = np.linspace(0,timeRange)
#plt.figure(figsize=(20, 12))
plt.xlabel('Time (years)', fontsize=15)
plt.ylabel(r'$Volume (x10^{-4} mm^3)$', fontsize=15)
plt.plot(timeData, volumeData, 'ro', linestyle='None')

#Chemotherapy - Sin^2 Function Approach
def chemoBefore(V, t): #Exponential model for phase 1
    expoParam = 1.5162879854524727
    dVdt = expoParam*V
    return dVdt
def chemoSinDiff(V,t): #Exponential model with sin^2 decay rate for phase 2
    expoParam = 1.5162879854524727
    timeParam = 15
    doseParam = 5
    dVdt = expoParam*V - doseParam*V*math.sin(timeParam*t)**2
    return dVdt
V0 = 0.0001 #Initial tumour volume
chemoTime = 5.5 #Time chemotherapy begins

if 100%timeRange == 0:
    numPointsBefore = (chemoTime/timeRange)*100
    numPointsAfter = 100 - numPointsBefore
elif 100%timeRange != 0:
    numPointsBefore = round((chemoTime/timeRange)*100)
    numPointsAfter = 100 - numPointsBefore
    
tArrBefore = np.linspace(0, chemoTime, int(numPointsBefore))
tArrAfter = np.linspace(chemoTime, timeRange, int(numPointsAfter))
chemoBefore = odeint(chemoBefore, V0, tArrBefore)
chemoAfter = odeint(chemoSinDiff, chemoBefore[-1], tArrAfter)

#Calculated volume and time arrays are joined together and plotted
chemo = []
time = []
for i in range(0, len(chemoBefore)):
    chemo.append(chemoBefore[i])
    time.append(tArrBefore[i])
for j in range(0, len(chemoAfter)):
    chemo.append(chemoAfter[j])
    time.append(tArrAfter[j])
plt.plot(time, chemo, label="New Model - Chemotherapy", color='darkturquoise')

plt.legend()
plt.show()
