from tabulate import tabulate
import numpy as np
import math

#Data used to determine parameters and for comparison
timeData = [0, 0.4630, 0.9260, 1.389, 1.852, 2.315, 2.736, 3.7287, 4.3424,  6.4602, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
logVolData = [math.log(x) for x in [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]]

#EXPO PARAMS
def getExpoParams(): #Works out the parameter p-q using each data point where t>0
    params = []
    for x in range(1,len(timeData)):
        params.append(np.log(volumeData[x]/0.0001)/timeData[x])
    return(params)

def expoVolLogEstimates(param): #Works out the estimated log volumes using the given parameter
    logVols = []
    for x in timeData:
        logVols.append(math.log(0.0001*math.exp(param*x)))
    return(logVols)
    
def residualsSquared(estiVols): #Calculates the squared difference between the estimated log vols and measured log vols
    resiArray = []
    for x in range(0,len(logVolData)):
        resiArray.append((estiVols[x]-logVolData[x])**2)
    return resiArray
    

expoParams = getExpoParams()
#Making Tables
print(tabulate({"Time": timeData, "Volume": volumeData, "p-q": ["-"]+expoParams}, headers="keys"),"\n")
print("A table to show the calculated parameter when using Vo and V at the given time")
print("------------------------------------------------------\n")

for i in range(0,10):
    parameter = expoParams[i]
    print("          p-q =", parameter)
    print(tabulate({"Time": timeData,"Estimated Log Vol.": expoVolLogEstimates(parameter), "Residual Squared": residualsSquared(expoVolLogEstimates(parameter))}, headers="keys"),"\n")
    print("Residual Sum of Squares = ", sum(residualsSquared(expoVolLogEstimates(parameter))))
    print("\n---------------------------------------------------")

