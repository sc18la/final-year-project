import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
import math

#DATA
timeData = [0, 0.463014, 0.926027, 1.38904, 1.85205, 2.31507, 2.73699, 3.728767, 4.342466,  6.460274, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
timeRange = 7
t = np.linspace(0,timeRange)
#plt.figure(figsize=(20, 12))
plt.xlabel('Time (years)', fontsize=15)
plt.ylabel(r'$Volume (x10^{-4} mm^3)$', fontsize=15)
plt.plot(timeData, volumeData, 'ro', linestyle='None')

#Surgery - Revised
def VSwitchFunc(t0, V0, tMax): #Returns the arrays of calculated volumes and their times using the volume threshold approach
    VSwitchVal = 2 #Volume value at which the model is changed
    def VSwitch(t, y):
        return y[0]-VSwitchVal
    VSwitch.terminal = True 
    
    def smallVSurgery(t, y): #Exponential model when the volume is small
        V = y[0]
        expoParam = 1.5162879854524727
        dVdt = expoParam*V
        return [dVdt]
    def largeVSurgery(t, y): #Gompertz model when the volume is large
        V = y[0]
        cLGomParam = 0.29473011238198343
        dLGomParam = 38.2
        dVdt = cLGomParam*V*math.log(dLGomParam/V)
        return [dVdt]
    
    #Arrays to hold results from solve_ivp
    VSurgery = [] 
    tSurgery = []
    func = smallVSurgery #func is the ODE given to solve_ivp. Initiated as the Expo model
    while True: 
        surgery = solve_ivp(func, (t0, tMax), V0, events=VSwitch)
        VSurgery.append(surgery.y)
        tSurgery.append(surgery.t)
        if surgery.status == 1: #When the event VSwitchFunc is triggered func is changed to the Gompertz Model
            func = largeVSurgery 
            t0 = surgery.t[-1]
            V0 = surgery.y[:, -1].copy()
        else:
            break
    return(VSurgery, tSurgery)

surgeryTime = 6.5 #Time of surgery
percentRemoved = 90 #Percent of tumour removed 

VSurgery = []
tSurgery = []
VSurBefore, tSurBefore = VSwitchFunc(0, [0.0001], surgeryTime)
VSurAfter, tSurAfter = VSwitchFunc(surgeryTime, [VSurBefore[0][0][-1]*(1-(percentRemoved/100))], timeRange)

#Calculated volume and time arrays are joined together and plotted
def makeArraysVol(Data): 
    arrData = []
    for i in range(0, len(Data)):
        for j in range(0, len(Data[i])):
            arrData.append(Data[i][j])
    return arrData

def makeArraysTime(Data):
    arrData = []
    for i in range(0, len(Data)):
        arrData.append(Data[i])
    return arrData

VSurBefore = np.concatenate(makeArraysVol(VSurBefore))
VSurAfter = np.concatenate(makeArraysVol(VSurAfter))
tSurBefore = np.concatenate(makeArraysTime(tSurBefore))
tSurAfter = np.concatenate(makeArraysTime(tSurAfter))
VSurgery.append(VSurBefore)
VSurgery.append(VSurAfter)
tSurgery.append(tSurBefore)
tSurgery.append(tSurAfter)

plt.plot(np.concatenate(tSurgery), np.concatenate(VSurgery), label="New Model - Surgery Revised", color="mediumslateblue")

plt.legend()
plt.show()