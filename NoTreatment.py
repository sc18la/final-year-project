import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import math

#DATA
timeData = [0, 0.463014, 0.926027, 1.38904, 1.85205, 2.31507, 2.73699, 3.728767, 4.342466,  6.460274, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
timeRange = 7
t = np.linspace(0,timeRange)
#plt.figure(figsize=(20, 12))
plt.xlabel('Time (years)', fontsize=15)
plt.ylabel(r'$Volume (x10^{-4} mm^3)$', fontsize=15)
plt.plot(timeData, volumeData, 'ro', linestyle='None')

#No Treatment
def early(V, t): #Exponetial model for phase 1
    expoParam = 1.5162879854524727
    dVdt = expoParam*V
    return dVdt
def later(V, t): #Gompertz model for phase 2
    cLGomParam = 0.29473
    dLGomParam = 38.2
    dVdt = cLGomParam*V*math.log(dLGomParam/V)
    return dVdt

V0 = 0.0001 #Initial tumour volume
switchTime = 6.2 #t_s (model switch time)
if 100%timeRange == 0:
    numPointsEarly = (switchTime/timeRange)*100
    numPointsLater = 100 - numPointsEarly
elif 100%timeRange != 0:
    numPointsEarly = round((switchTime/timeRange)*100)
    numPointsLater = 100 - numPointsEarly
    
tEarly = np.linspace(0, switchTime, int(numPointsEarly))
tLater = np.linspace(switchTime, timeRange, int(numPointsLater))
noTreatmentEarly = odeint(early, V0, tEarly)
noTreatmentLater = odeint(later, noTreatmentEarly[-1], tLater)

#Calculated volume and time arrays are joined together and plotted
noTreatment = []
tNoTreatment = []
for i in range(0, len(noTreatmentEarly)):
    noTreatment.append(noTreatmentEarly[i])
    tNoTreatment.append(tEarly[i])
for j in range(0, len(noTreatmentLater)):
    noTreatment.append(noTreatmentLater[j])
    tNoTreatment.append(tLater[j])
plt.plot(tNoTreatment, np.concatenate(noTreatment), label="New Model - No Treatment", color='grey')
    

plt.legend()
plt.show()